import Banner from '../components/Banner.js';






export default function Error(){

    const data = {
	    imgSrc: false,
	    content: "The page you are looking for is not found",
	    destination: "/",
	    label: "Go back"
	}

	return(
		<>
			<Banner data={data} />
		</>
	)
}