import { useContext } from 'react';
import UserContext from '../UserContext';
import ProfileAdmin from '../components/ProfileAdmin.js';
import ProfileUser from '../components/ProfileUser.js';


export default function Profile() {
    const { user } = useContext(UserContext);


    return(
        <>
        
            {
                user.isAdmin ? 
                    (
                        <ProfileAdmin />
                    ) 
                : 
                    (
                        <ProfileUser />
                    )
            }
        </>
    )
}