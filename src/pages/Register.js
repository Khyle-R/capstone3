import { useState, useEffect, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Card, InputGroup } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){

	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState();
	const [city, setCity] = useState('');
	const [address, setAddress] = useState('');
  	const [password, setPassword] = useState('');
  	const [confirmPassword, setConfirmPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [registrationSuccess, setRegistrationSuccess] = useState(false);

	function registerUser(event){
		
		// Prevents page load upon form submission
		event.preventDefault();

		// Sends a request to the /register endpoint which will include all the fields necessary for that route.
		fetch('https://produce-commerce.onrender.com/api/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				mobileNo: mobileNo,
				city: city,
				address: address,
				password: password
			})
		}).then(response => response.json()).then(result => {
			if(result){
				// Resets all input fields
				setEmail("")
				setPassword("")
				setConfirmPassword("")

				Swal.fire({
					icon: 'success',
					title: 'Register Successful',
				})
				setRegistrationSuccess(true);
				
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Oops... Something went wrong!',
					text: 'Please try again',
				})
			}
		})
	}

	
	
	// The useEffect arrow function will trigger everytime there are changes in the data within the 2nd argument array.
	// Note: If the 2nd argument array is empty, then the function will only run upon the initial loading of the component.
	useEffect(() => {
		// Checks if all fields aren't empty, if password and confirm password fields are matching, and mobile number is 11 characters.
		if((email !== "" && password !== "" && confirmPassword !== "" && mobileNo !== "" && city !== "" && address !== "") && (password === confirmPassword) && (mobileNo.length === 10)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password, confirmPassword, mobileNo, address, city]);

	if (registrationSuccess) {
		return <Navigate to="/login" />;
	}

	return(

		(user.id != null) ?
            <Navigate to="/" />
        :
		<Container className="pt-4">
			<Row className="justify-content-center">
				<Col md={6}>
					<Card border="success" className='p-2' >
						<Card.Body>
							<Card.Title className="mb-3 fs-2 fw-bold text-center">Register</Card.Title>
							<Form onSubmit={(event) => registerUser(event)}>
								<Form.Group controlId="email">
									<Form.Label>Email</Form.Label>
									<Form.Control
										// placeholder="ex. JohnDoe@gmail.com"
										type="email"
										value={email}
										onChange={(event) => setEmail(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="mobileNo">
									<Form.Label>Mobile #</Form.Label>
									<InputGroup className="mb-3">
										
										<InputGroup.Text id="basic-addon1">+63</InputGroup.Text>
										<Form.Control
											// placeholder="ex. 9562909065"
											type="number"
											value={mobileNo}
											onChange={(event) => setMobileNo(event.target.value)}
											required
										/>
									</InputGroup>
								</Form.Group>

								<Form.Group controlId="city">
									<Form.Label>City</Form.Label>
									<Form.Control
										// placeholder="ex. Makati City"
										type="text"
										value={city}
										onChange={(event) => setCity(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="address">
									<Form.Label>Address</Form.Label>
									<Form.Control
									placeholder="ex. Blk 2 Lot 8 brgy 227"
										type="text"
										value={address}
										onChange={(event) => setAddress(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password">
									<Form.Label>Password</Form.Label>
									<Form.Control
									type="password"
									value={password}
									onChange={(event) => setPassword(event.target.value)}
									required
									/>
								</Form.Group>

								<Form.Group controlId="confirmPassword">
									<Form.Label>Confirm Password</Form.Label>
									<Form.Control
									type="password"
									value={confirmPassword}
									onChange={(event) => setConfirmPassword(event.target.value)}
									required
									/>
								</Form.Group>
								
								<div className="d-flex flex-row justify-content-between align-items-center">
									<div className="mt-3">
										Already have an Account? <Link to='/login'> Login </Link>
									</div>	
									<Button className="mt-3" variant="success" type="submit" disabled={isActive === false}>
										Submit
									</Button>
								</div>		
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
	  	</Container>
	)
}