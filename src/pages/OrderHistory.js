import { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import OrdersAdmin from '../components/OrdersAdmin';
import OrdersUser from '../components/OrdersUser';



export default function Product() {
    const { user } = useContext(UserContext);

    console.log(user)
    if (user.id === null) {
        return <Navigate to="/login" />;
    }
    return(
        <>
        
            {
                user.isAdmin ? 
                    (
                        <OrdersAdmin />
                    ) 
                : 
                    (
                        <OrdersUser />
                    )
            }
        </>
    )
}