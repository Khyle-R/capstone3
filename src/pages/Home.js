import Banner from '../components/Banner.js';
import { Carousel, Card, Container, Row, Col } from 'react-bootstrap';
import meat from '../images/meat.png';
import fruit from '../images/fruit.png';
import vegetables from '../images/vegetables.png';
import './pages.css'



export default function Home(){
	
	const data = {
	    imgSrc: true,
	    content: "Best Quality Products Delivered at Your Doorstep!",
	    destination: "/products",
	    label: "Get Started"
	}

	return(
		<>
			<Banner data={data}/>

			<Container>
				<Carousel>
					<Carousel.Item>
						<Row className="justify-content-center">
							<Col sm={6}>
								<Card>
									<Card.Img variant="top" src={fruit} className="carousel-image"/>
								</Card>
							</Col>
						</Row>
					</Carousel.Item>
					<Carousel.Item>
						<Row className="justify-content-center">
							<Col sm={6}>
								<Card>
									<Card.Img variant="top" src={meat} className="carousel-image"/>
								</Card>
							</Col>
						</Row>
					</Carousel.Item>
					<Carousel.Item>
						<Row className="justify-content-center">
							<Col sm={6}>
								<Card>
									<Card.Img variant="top" src={vegetables} className="carousel-image"/>
								</Card>
							</Col>
						</Row>
					</Carousel.Item>
				</Carousel>
			</Container>
		</>
	)
}