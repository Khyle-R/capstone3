import { useState, useEffect, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Login(){
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
  	const [password, setPassword] = useState('');
  	const [isActive, setIsActive] = useState(false);

	function authenticate(event){
		// Prevents page load upon form submission
		event.preventDefault();

		// Sends a request to the /register endpoint which will include all the fields necessary for that route.
		fetch('https://produce-commerce.onrender.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
        .then(response => response.json())
        .then(data => {
            console.log(data.accessToken)
			if(data.accessToken){
				// Resets all input fields

                localStorage.setItem('token', data.accessToken);
				setEmail("")
				setPassword("")
                
                retrieveUserDetails(data.accessToken)
				Swal.fire({
					icon: 'success',
					title: 'Login Successful',
					text: 'Have a nice day',
				})
			} else {
				Swal.fire({
					title: 'Oops... Something went wrong!',
					icon: 'error',
					text: 'Check if your email and password is correct',
				})
			}
		})
	}

    const retrieveUserDetails = (token) => {

        fetch('https://produce-commerce.onrender.com/api/users/details', {
            headers: {
                'Authorization': `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return(

		(user.id != null) ?
            <Navigate to="/" />
        :
		<Container className="pt-5 justify-content-center">
			<Row className="justify-content-center">
				<Col md={6}>
					<Card border="success" className='p-2' >
						<Card.Body>
							<Card.Title className="mb-4 fs-2 fw-bold text-center">Login</Card.Title>
							<Form onSubmit={(event) => authenticate(event)}>
								<Form.Group controlId="email">
									<Form.Label>Email</Form.Label>
									<Form.Control
										type="email"
										value={email}
										onChange={(event) => setEmail(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password">
									<Form.Label>Password</Form.Label>
									<Form.Control
										type="password"
										value={password}
										onChange={(event) => setPassword(event.target.value)}
										required
									/>
								</Form.Group>
								
								<div className="d-flex flex-row justify-content-between align-items-center">
									<div className="mt-3">
										Do you have an account? <Link to='/register'> Register </Link>
									</div>	
									<Button className="mt-3" variant="success" type="submit" disabled={isActive === false}>
										Submit
									</Button>
								</div>		

							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
	  	</Container>
	)
}