import { useContext } from 'react';
import UserContext from '../UserContext';
import ProductsAdmin from '../components/ProductsAdmin';
import ProductCard from "../components/ProductsUser";



export default function Product() {
    const { user } = useContext(UserContext);

    return(
        <>
            {
                user.isAdmin ? 
                    (
                        <ProductsAdmin />
                    ) 
                : 
                    (
                        <ProductCard />
                    )
            }
        </>
    )
}