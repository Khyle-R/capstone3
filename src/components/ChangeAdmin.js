import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';




export default function ChangeAdmin({item, isAdmin, fetchData}){

	const activateUser = () => {

		fetch(`https://produce-commerce.onrender.com/api/users/${item}/active`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User is now an Admin'
				})

				fetchData();
				
			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
                fetchData();
			}
		})
	}


	return (

		(isAdmin)?
			<Button variant="secondary" size="sm"disabled="true"> Change to Admin </Button>
		:
			<Button variant="success" size="sm" onClick={() => activateUser()}> Change to Admin </Button>

	)
}