import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
	



export default  function EditProduct({fetchData}){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [showAdd, setShowAdd] = useState(false);

	// const openEdit = () => {
	// 	setShowEdit(true);
	// }

	const closeAdd = () => {
		setShowAdd(false);
		setName('');
		setDescription('');
		setPrice(0);
	}

	const addProduct = (e) => {
		e.preventDefault();

		fetch(`https://produce-commerce.onrender.com/api/products/add/`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Added sucessfully'
				})
				closeAdd();
				fetchData();
			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
				closeAdd();
                fetchData();
			}
		})
	}

	return(
		<>
            <div className="d-flex justify-content-end">
			    <Button 
                    className="ml-auto mb-3" 
                    variant="success" size="md" 
                    onClick={() => setShowAdd(true)} 
                > 
                    Add Product 
                     
                </Button>
            </div>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title> Add Product </Modal.Title>
					</Modal.Header>
					
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label> Name </Form.Label>
							<Form.Control 
								type="text" required
								onChange={e => setName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label> Description </Form.Label>
							<Form.Control 
								type="text" required 
								onChange={e => setDescription(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label> Price </Form.Label>
							<Form.Control 
								type="number" required 
								onChange={e => setPrice(e.target.value)}
							/>
						</Form.Group>	

					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}> Close </Button>
						<Button variant="success" type="submit"> Add </Button>
					</Modal.Footer>

				</Form>
			</Modal>

		</>
	)
}