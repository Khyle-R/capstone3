import { useState, useContext } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import './components.css';



export default  function Order({product}){

    const { user } = useContext(UserContext);
	const [productId, setProductId] = useState('');

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);

	const [showOrder, setShowOrder] = useState(false);

	const openOrder = (productID) => {
		fetch(`https://produce-commerce.onrender.com/api/products/get/${productID}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			console.log(data);
		})

		setShowOrder(true);
	}

	const closeOrder = () => {
		setShowOrder(false);
		setName('');
		setDescription('');
		setPrice(0);
	}

	const addOrder = (e) => {
		e.preventDefault();

		fetch(`https://produce-commerce.onrender.com/api/orders/add`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				productName: name,
                quantity: quantity,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {

            if(result){

                Swal.fire({
                    icon: 'success',
                    title: 'Order Successful',
                })
                setShowOrder(false);
                console.log(result)
                
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops... Something went wrong!',
                    text: 'Please try again',
                })
            }
        })
	}

	return(
		<>
			<Button variant="success" size="sm" onClick={() => openOrder(product)} > Details </Button>

			<Modal show={showOrder} onHide={closeOrder}>
				<Form onSubmit={e => addOrder(e)}>
					<Modal.Header closeButton>
						<Modal.Title> Add Order </Modal.Title>
					</Modal.Header>
					
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label> Product: {name} </Form.Label>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label> Description: {description} </Form.Label>
						</Form.Group>

						<Form.Group controlId="coursePrice">
							<Form.Label> Price: ₱{price} </Form.Label>
						</Form.Group>	

                        <Form.Group controlId="productQuantity">
							<Form.Label> Quantity: </Form.Label>
							<Form.Control 
								type="number" required
								min={1}
								value={quantity}
								onChange={e => {
									const inputValue = parseInt(e.target.value, 10);
									
									if (!isNaN(inputValue) && inputValue >= 0) {
									  setQuantity(inputValue);
									}
								}}
							/>
						</Form.Group>

					</Modal.Body>
					<Modal.Footer>

						<div className="footer-left">
							<Form.Group controlId="courseTotal">
								<Form.Label> Total: ₱{price * quantity}</Form.Label>
							</Form.Group>
						</div>

						<div>
							<Button variant="secondary footer-link" onClick={closeOrder}> Close </Button>
							{
								user.id !== null ?
									<Button variant="success" type="submit"> Order </Button>
								:
									<Link className="btn btn-danger" to="/login">Login</Link>
							}
						</div>
					</Modal.Footer>

				</Form>
			</Modal>

		</>
	)
}