import { useEffect, useState } from 'react';
import ChangeAdmin from './ChangeAdmin';



export default function ProfileAdmin() {
    const [user, setUser] = useState([])
    const [userData, setUserData] = useState([])

    const fetchData = () => {
    	fetch(`https://produce-commerce.onrender.com/api/users/getAll`, {
            method: "GET",
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }})
      	.then(res => res.json())
    	.then(data => {
        	//console.log(data);
        	setUserData(data);
      	});
  	};

  	useEffect(() => {

        fetchData();

    }, [])
  	


	
	useEffect(() => {
		const userHolder = userData.map(item => {
			return (
				<tr key={item._id}>
	            	<td>{item._id}</td>
	              	<td>{item.email}</td>
	              	<td>{item.mobileNo}</td>
	              	<td>
		              	{
		              		item.isAdmin ? 
		              		(
		              			<p className="link-success"> Administrator </p>
		              		)
		              		:
		              		(
		              			<p className="link-warning"> User </p>
		              		)
		              	}
	              	</td>
		            <td>
		            	<ChangeAdmin item={item._id} isAdmin={item.isAdmin} fetchData={fetchData} />
		            </td>
            	</tr>
			)
		})

		setUser(userHolder)
	}, [userData])

	return (
	    <div>
	      	<h2 className="text-center mt-4 mb-4">Admin Dashboard</h2>
	      		
      		<table className="table table-striped table-bordered table-hover table-responsive">
        		<thead className="text-center">
          			<tr>
			            <th>User ID</th>
			            <th>Email</th>
			            <th>Mobile No. </th>
			            <th>User Type</th>
			            <th>Action</th>
		          	</tr>
        		</thead>
        		<tbody className="text-center">
          
        		    { user }

        		</tbody>
      		</table>
		</div>
  );
}


