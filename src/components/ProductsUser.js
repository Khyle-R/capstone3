import React, { useState, useEffect } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import Order from '../components/Order'




export default function ProductCard(){
    const [productData, setproductData] = useState([]);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://produce-commerce.onrender.com/api/products/getActive')
        .then(response => response.json())
        .then(data => setproductData(data))
        .catch(error => console.error('Error fetching data:', error));
    }, []);

      useEffect(() => {
		const productHolder = productData.map(item => {
			return (
                <Col key={item._id} xs={12} sm={6} md={4} lg={3}> 
                        <Card className="mb-4">
                            <Card.Img variant="top" src="" />
                            <Card.Body>
                                <Card.Title>{item.name}</Card.Title>
                                <Card.Text>{item.description}</Card.Text>
                                <p>₱{item.price}</p>
                            </Card.Body>
                            <Card.Footer>
                                <Order product={item._id}/>
                            </Card.Footer>
                        </Card>
                    </Col>
            )
		})

		setProducts(productHolder)
	}, [productData])

    return (
        <div>
            <h1 className="text-center mt-3 mb-4">Products</h1>
            <Row>
                { products }
            </Row>
        </div>
    )
};
