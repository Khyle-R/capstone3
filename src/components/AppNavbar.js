import { useContext } from 'react';
import UserContext from '../UserContext';
import {Nav, Navbar, Container} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import logo from '../images/logo.png';


import './components.css';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar collapseOnSelect expand="lg" className="bg-3d-gradient mb-2">
			<Container className="fluid">
				{/* <Navbar.Brand as={NavLink} to="/">Produce To Go</Navbar.Brand> */}
				<Navbar.Brand as={NavLink} to="/" className="d-flex align-items-center">
					<img src={logo} alt="Produce To Go Logo" className="logo-image" />
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="responsive-navbar-nav" />
				<Navbar.Collapse id="responsive-navbar-nav">

				<Nav className="me-auto"></Nav>
				
				<Nav className="navbar-nav-center">
					<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					<Nav.Link as={NavLink} to="/orders"> Orders </Nav.Link>

				{(user.id !==null) ?
					<>
						<Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
					</>
				:
					<>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
					</>
				}
				</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
  );
}