import { useEffect, useState } from 'react';






export default function ProductsAdmin() {
    const [orders, setOrder] = useState([])
    const [orderData, setOrderData] = useState([])

    const fetchData = () => {
    	fetch(`https://produce-commerce.onrender.com/api/orders/getAll`, {
            method: "GET",
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }})
      	.then(res => res.json())
    	.then(data => {
        	console.log(data);
        	setOrderData(data);
      	});
  	};

  	useEffect(() => {
  		fetchData();
  	}, []);

	
	useEffect(() => {
		const orderHolder = orderData.map(item => {
			return (
				<tr key={item._id}>
                    <td>{item._id}</td>
	            	<td>{item.userId}</td>
	              	<td>{item.productId}</td>
	              	<td>{item.quantity}</td>
	              	<td>{item.totalAmount}</td>
	              	<td>{item.purchasedOn}</td>    
            	</tr>
			)
		})

		setOrder(orderHolder)
	}, [orderData])

	return (
	    <div>
	      	<h2 className="text-center mt-4 mb-4">All Orders</h2>
	      		
      		<table className="table table-striped table-bordered table-hover table-responsive">
        		<thead className="text-center">
          			<tr>
			            <th>ID</th>
			            <th>User ID</th>
			            <th>Product ID</th>
			            <th>Quantity</th>
			            <th>Total Amount</th>
			            <th>Purchase Date</th>
		          	</tr>
        		</thead>
        		<tbody className="text-center">
          
        		    { orders }

        		</tbody>
      		</table>
		</div>
  );
}


