import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';




export default function Archive({item, isActive, fetchData}){
	
	const archiveProduct = () => {

		fetch(`https://produce-commerce.onrender.com/api/products/${item}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Archived'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
			
			}
		})
	}

	const activateProduct = () => {

		fetch(`https://produce-commerce.onrender.com/api/products/${item}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Activated'
				})

				fetchData();
				
			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
			
			}
		})
	}


	return (

		(isActive)?
			<Button variant="danger" size="sm" onClick={() => archiveProduct()}> Archive </Button>
		:
			<Button variant="primary" size="sm" onClick={() => activateProduct()}> Activate </Button>

	)
}