import React, { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import './components.css';




export default function ProfileUser() {
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState(0);
    const [city, setCity] = useState('');
    const [address, setAddress] = useState('');

    const fetchData = () => {
        fetch(`https://produce-commerce.onrender.com/api/users/details`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // Assuming the fetched data contains user details
            setEmail(data.email);
            setMobileNo(data.mobileNo);
            setCity(data.city);
            setAddress(data.address);
        });
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="d-flex justify-content-center align-items-center">
            <Card className="bigger-card">
                <Card.Body className="profile-card-body">
                    <Card.Text className="profile-head">User Profile</Card.Text>
                    <Card.Text className="profile-text">Email: {email}</Card.Text>
                    <Card.Text className="profile-text">Mobile: +63 {mobileNo}</Card.Text>
                    <Card.Text className="profile-text">City: {city}</Card.Text>
                    <Card.Text className="profile-text">Address: {address}</Card.Text>
                </Card.Body>
            </Card>
        </div>
    );
}
