import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';





export default function ProductsAdmin() {
    const { user } = useContext(UserContext);
    const [orders, setOrder] = useState([])
    const [orderData, setOrderData] = useState([])

    const fetchData = () => {
    	fetch(`https://produce-commerce.onrender.com/api/orders/get/${user.id}`, {
            method: "GET",
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }})
      	.then(res => res.json())
    	.then(data => {
        	// console.log(data);
        	setOrderData(data);
      	});
  	};

  	fetchData();
	
	useEffect(() => {
		const orderHolder = orderData.map(item => {
			return (
				<tr key={item._id}>
                    <td>{item._id}</td>
	              	<td>{item.productName}</td>
	              	<td>{item.quantity}</td>
	              	<td>{item.totalAmount}</td>
	              	<td>{item.purchasedOn}</td>    
            	</tr>
			)
		})

		setOrder(orderHolder)
	}, [orderData])

	return (
	    <div>
	      	<h2 className="text-center mt-4 mb-4">Your Orders</h2>
	      		
      		<table className="table table-striped table-bordered table-hover table-responsive">
        		<thead className="text-center">
          			<tr>
			            <th>Order ID</th>
			            <th>Product Name</th>
			            <th>Quantity</th>
			            <th>Total Amount</th>
			            <th>Purchase Date</th>
		          	</tr>
        		</thead>
        		<tbody className="text-center">
          
        		    { orders }

        		</tbody>
      		</table>
		</div>
  );
}


