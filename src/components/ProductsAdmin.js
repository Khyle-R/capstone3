import { useEffect, useState } from 'react';
import AddProduct from 	'./AddProduct';
import EditProduct from './EditProduct';
import Archive from './Archive';



export default function ProductsAdmin() {
    const [products, setProducts] = useState([])
    const [productData, setProductData] = useState([])

    const fetchData = () => {
    	fetch(`https://produce-commerce.onrender.com/api/products/getAll`)
      	.then(res => res.json())
    	.then(data => {
        	//console.log(data);
        	setProductData(data);
      	});
  	};

  	useEffect(() => {

        fetchData();

    }, [])

	
	useEffect(() => {
		const productHolder = productData.map(item => {
			return (
				<tr key={item._id}>
	            	<td>{item._id}</td>
	              	<td>{item.name}</td>
	              	<td>{item.description}</td>
	              	<td>{item.price}</td>
	              	<td>
		              	{
		              		item.isActive ? 
		              		(
		              			<p className="link-success"> Available </p>
		              		)
		              		:
		              		(
		              			<p className="link-danger"> Unavailable </p>
		              		)
		              	}
	              	</td>
		            <td>
		            	<EditProduct item={item._id} fetchData={fetchData} />
		            </td>
		            <td>
		            	<Archive item={item._id} isActive={item.isActive} fetchData={fetchData} />
		            </td>
            	</tr>
			)
		})

		setProducts(productHolder)
	}, [productData])

	return (
	    <div>
	      	<h2 className="text-center mt-4 mb-4">Admin Dashboard</h2>
	      		
			<AddProduct fetchData={fetchData} />
      		<table className="table table-striped table-bordered table-hover table-responsive">
        		<thead className="text-center">
          			<tr>
			            <th>ID</th>
			            <th>Name</th>
			            <th>Description</th>
			            <th>Price</th>
			            <th>Availability</th>
			            <th colSpan="2">Actions</th>
		          	</tr>
        		</thead>
        		<tbody className="text-center">
          
        		    { products }

        		</tbody>
      		</table>
		</div>
  );
}


