import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import error from '../images/error.png';
import logo from '../images/logo.png';





export default function Banner({data}){

	
	const {content, destination, imgSrc, label} = data;
	
	return(
		<Row className="justify-content-center">
			<Col xs={12} md={6} className="p-5">
				<div className="banner-container">
					<img src={imgSrc ? logo : error} alt="banner" className="banner-logo" />
					
					<h5>{content}</h5>

					<Link className="btn btn-success btn-lg mt-3" to={destination}>
						{label}
					</Link>
				</div>
			</Col>
	  	</Row>
		
	)
}