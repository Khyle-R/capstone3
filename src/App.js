import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Product from './pages/Product.js';
import OrderHistory from './pages/OrderHistory.js';
import Profile from './pages/Profile.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js'
import Register from './pages/Register.js';
import Error from './pages/Error.js';
import './App.css';

export default function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    //Function for clearing localStorage upon logout.
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])


    useEffect(() => {
        fetch('https://produce-commerce.onrender.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }
        })
    }, [])

    return (
        <>
            
            <UserProvider value={{user, setUser, unsetUser}}>
                <Router>
                    <AppNavbar/>
                    <Container>
                        <Routes>
                            <Route path="/" element={<Home />} /> 
                            <Route path="/products" element={<Product />} />
                            <Route path="/orders" element={<OrderHistory />} />
                            <Route path="/profile" element={<Profile />} />
                            <Route path="/login" element={<Login />} />
                            <Route path="/register" element={<Register />} />
                            <Route path="/logout" element={<Logout />} /> 
                            <Route path="*" element={<Error />} />
                        </Routes>
                    </Container>
                </Router>
            </UserProvider>
        </>
    );
}